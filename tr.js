const token = require('google-translate-token');
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;


// process.argv
src = process.argv[2];
dest = process.argv[3];
text = process.argv[4];
if (process.argv[5] != null)
  max = process.argv[5];
else
  max = 5;

token.get(text).then(result => {
  if (result) {
    parse(result.name, result.value);
  } else {
    console.log("ERROR");
  }
});

function parse(tk_name, tk_value) {
  var xmlHttp = new XMLHttpRequest();
  var theUrl = 'https://translate.google.com/translate_a/single?client=webapp&sl=' + src + '&tl=' + dest + '&hl=en&dt=at&dt=bd&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=t&dt=gt&ssel=0&tsel=0&kc=1&' + tk_name + '=' + tk_value + '&q=' + text;
  // console.log(theUrl);
  xmlHttp.open("Get", theUrl, false);
  xmlHttp.send(null);
  var response = xmlHttp.responseText;
  var obj = JSON.parse(response);
  console.log(obj[0][0][0]);

  // source language
  console.log(obj[2]);

  // spell checker
  if (obj[7] != null) {
    console.log(obj[7][1]);
  } else {
    console.log("");
  }
  
  // pronunciation
  if (obj[0][1] != null)
    console.log(obj[0][1][3]);
  else
    console.log("");

  // destination translations
  if (obj[1] != null) {
    for (var i = 0; i < obj[1].length;  i++) {
      process.stdout.write(obj[1][i][0] + " | ");
      for (var j = 0; j < obj[1][i][2].length && j < max; j++) {
        process.stdout.write(obj[1][i][2][j][0] + " :: ");
        process.stdout.write(obj[1][i][2][j][1].toString() + " | ");
      }
      process.stdout.write("\n");
    }
  }

  // source translations
  if (obj[2] == dest && obj[12] != null) {
    for (var i = 0; i < obj[12].length;  i++) {
      process.stdout.write(obj[12][i][0] + " | ");
      for (var j = 0; j < obj[12][i][1].length && j < max; j++) {
        process.stdout.write(obj[12][i][1][j][0] + " | ");
      }
      process.stdout.write("\n");
    }
  }
}
