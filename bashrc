# /etc/bash/bashrc
#
# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output.  So make sure this doesn't display
# anything or bad things will happen !

set -o vi

# Syntactic sugar for ANSI escape sequences
# txtblk='\e[0;30m' # Black - Regular
# txtred='\e[0;31m' # Red
# txtgrn='\e[0;32m' # Green
# txtylw='\e[0;33m' # Yellow
# txtblu='\e[0;34m' # Blue
# txtpur='\e[0;35m' # Purple
# txtcyn='\e[0;36m' # Cyan
# txtwht='\e[0;37m' # White
# bldblk='\e[1;30m' # Black - Bold
# bldred='\e[1;31m' # Red
# bldgrn='\e[1;32m' # Green
# bldylw='\e[1;33m' # Yellow
# bldblu='\e[1;34m' # Blue
# bldpur='\e[1;35m' # Purple
# bldcyn='\e[1;36m' # Cyan
# bldwht='\e[1;37m' # White
# unkblk='\e[4;30m' # Black - Underline
# undred='\e[4;31m' # Red
# undgrn='\e[4;32m' # Green
# undylw='\e[4;33m' # Yellow
# undblu='\e[4;34m' # Blue
# undpur='\e[4;35m' # Purple
# undcyn='\e[4;36m' # Cyan
# undwht='\e[4;37m' # White
# bakblk='\e[40m'   # Black - Background
# bakred='\e[41m'   # Red
# badgrn='\e[42m'   # Green
# bakylw='\e[43m'   # Yellow
# bakblu='\e[44m'   # Blue
# bakpur='\e[45m'   # Purple
# bakcyn='\e[46m'   # Cyan
# bakwht='\e[47m'   # White
# txtrst='\e[0m'    # Text Reset


# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.


# if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	# return
# fi

# Bash won't get SIGWINCH if another process is in the foreground.
# Enable checkwinsize so that bash will check the terminal size when
# it regains control.  #65623
# http://cnswww.cns.cwru.edu/~chet/bash/FAQ (E11)
shopt -s checkwinsize

# Enable history appending instead of overwriting.  #139609
shopt -s histappend

# Change the window title of X terminals 
case ${TERM} in
	xterm*|rxvt*|Eterm|aterm|kterm|gnome*|interix)
		PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/$HOME/~}\007"'
		;;
	screen)
		PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/$HOME/~}\033\\"'
		;;
esac

# use_color=false

# Set colorful PS1 only on colorful terminals.
# dircolors --print-database uses its own built-in database
# instead of using /etc/DIR_COLORS.  Try to use the external file
# first to take advantage of user additions.  Use internal bash
# globbing instead of external grep binary.
safe_term=${TERM//[^[:alnum:]]/?}   # sanitize TERM
match_lhs=""
[[ -f ~/.dir_colors   ]] && match_lhs="${match_lhs}$(<~/.dir_colors)"
[[ -f /etc/DIR_COLORS ]] && match_lhs="${match_lhs}$(</etc/DIR_COLORS)"
[[ -z ${match_lhs}    ]] \
	&& type -P dircolors >/dev/null \
	&& match_lhs=$(dircolors --print-database)
[[ $'\n'${match_lhs} == *$'\n'"TERM "${safe_term}* ]] && use_color=true

if ${use_color} ; then
	# Enable colors for ls, etc.  Prefer ~/.dir_colors #64489
	if type -P dircolors >/dev/null ; then
		if [[ -f ~/.dir_colors ]] ; then
			eval $(dircolors -b ~/.dir_colors)
		elif [[ -f /etc/DIR_COLORS ]] ; then
			eval $(dircolors -b /etc/DIR_COLORS)
		fi
	fi

# 	if [[ ${EUID} == 0 ]] ; then
# 		PS1='\[\033[01;31m\]\h\[\033[01;34m\] \W \$\[\033[00m\] '
# 	else
# 		PS1='\[\033[01;32m\]\u@\h\[\033[01;34m\] \w \$\[\033[00m\] '
# 	fi
#
        P1="\$(if [[ \$? == 0 ]]; then echo \"\[\e[1;32m\]\342\234\223\"; else echo \"\[\e[1;31m\]\342\234\227\"; fi) \[\033[01;32m\]\u \[\033[1;34m\] \W "
        P2='\[\033[01;35m\]$(__git_ps1 " [%s] ")\[\033[1;34m\]\$\[\033[00m\] '
        PS1="$P1$P2"
        
	alias ls='ls --color=auto'
	alias grep='grep --colour=auto'
else
	if [[ ${EUID} == 0 ]] ; then
		# show root@ when we don't have colors
		PS1='\u@\h \W \$ '
	else
		PS1='\u@\h \w \$ '
	fi
fi

########################GIT########################
source ~/bin/git-prompt.sh
########################GIT########################

########################Alias########################
alias ll='ls -al'
########################Alias########################

############################ Try to keep environment pollution down, EPA loves us.
unset use_color safe_term match_lhs
alias :q=exit
alias actcb="source /home/mostafa/github/cb-03/venv/bin/activate"
############################ Try to keep environment pollution down, EPA loves us.

######################################## Unlimited bash history
HISTSIZE=2000
HISTFILESIZE=2000
######################################## Unlimited bash history

#################################################### fzf in bash
set -o vi
source /usr/share/fzf/key-bindings.bash
source /usr/share/fzf/completion.bash
export FZF_ALT_C_OPTS="--preview 'tree -C {} | head -200'"

# Use fd to generate the list for directory completion
_fzf_compgen_path() {
  fd --hidden --exclude ".git" . "$1"
}
_fzf_compgen_dir() {
  fd --type d --follow --exclude ".git" . "$1"
}

# Not trigger ** from path completion for certain commands
_fzf_complete_file_path_notrigger() {
    FZF_COMPLETION_TRIGGER='' _fzf_file_completion
}
_fzf_complete_dir_path_notrigger() {
    FZF_COMPLETION_TRIGGER='' _fzf_dir_completion
}
command complete -o bashdefault -o default -F _fzf_complete_file_path_notrigger vim
complete -o bashdefault -o default -F _fzf_complete_dir_path_notrigger cd
#################################################### fzf in bash
