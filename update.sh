#!/bin/bash

# update vimrc
cp /home/mostafa/.vimrc vim/vimrc

# update vim/colors
cp -r /home/mostafa/.vim/colors vim/

# update vim/ftplugin
cp -r /home/mostafa/.vim/ftplugin vim/

# update vim/syntax
cp -r /home/mostafa/.vim/syntax vim/

# update bashrc
cp /home/mostafa/.bashrc bashrc

# update tr.js
cp /home/mostafa/tr.js tr.js
