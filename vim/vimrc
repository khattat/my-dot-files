" /usr/share/vim/vimfiles/archlinux.vim) and sourced by the call to :runtime
" you can find below.  If you wish to change any of those settings, you should
" do it in this file (/etc/vimrc), since archlinux.vim will be overwritten
" everytime an upgrade of the vim packages is performed.  It is recommended to
" make changes after sourcing archlinux.vim since it alters the value of the
" 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages.  runtime! archlinux.vim

" If you prefer the old-style vim functionalty, add 'runtime! vimrc_example.vim'
" Or better yet, read /usr/share/vim/vim74/vimrc_example.vim or the vim manual
" and configure vim to your own liking!
"":hi Indetifier guifg=palegreen


"---------------------Key Maps----------------------------"
let mapleader = ","
map <F5> :NERDTreeToggle<CR>
"Open konsole at the current directory of opened file
map <A-F12> :!konsole --workdir %:p:h & <CR><CR>
" fzf shortcut
nnoremap <leader>. :Files<CR>
" Tags
nnoremap <leader>c :Tags <CR>
" Git grep
nnoremap <leader>g :Rg <CR>
" Git search buffer
nnoremap <leader>/ :BLines <CR>

" Prepear the list of buffers for you
nnoremap <Leader>b :ls<CR>:b<Space>
" Prepear the list of buffers in ctrlp plugin
nnoremap <Leader>l :Buffers<CR>
" FixIt command of YouCompleteME
nnoremap <Leader>f :YcmCompleter FixIt<CR>
" Ctrl-B to break and enter
"inoremap <C-b> <CR><ESC><S-o><SPACE><SPACE><SPACE><SPACE>
inoremap <C-b> <CR><ESC><S-o>
" exit a buffer with leader space
nnoremap <Space>q :q<CR>
" Nerd tree find the current file
nnoremap <Leader>n :NERDTreeFind<CR>
" remap scroll up and down
nnoremap <S-K> <C-Y>
nnoremap <S-J> <C-E>
" remap Ctrl-R
nnoremap <C-E> <C-R>
" paste from clipboard
nnoremap <Leader>p "+p
" Run ALELint to check syntax manually
nnoremap <Leader>; :ALELint<CR>

"----Switch between split windows-----"
nnoremap <C-J> <C-W>j
nnoremap <C-K> <C-W>k
nnoremap <C-L> <C-W>l
nnoremap <C-H> <C-W>h
"----End Switch between split windows---"

" Move Mode
" nnoremap <C-M> :WinResizerStartMove<CR>
" Resize Mode
let g:winresizer_start_key = '<C-R>'
" Go to the previous tab
nnoremap gh :tabprevious<CR>
" Create an empty tab
nnoremap <C-T> :tabedit<CR>
" Expand opening-brace followed by ENTER to a block and place cursor inside
inoremap {<CR> {<CR>}<Esc>O
"press return to temporarily get out of the highlighted search.
nnoremap <TAB> :nohlsearch<CR>
"Tagbar
nnoremap <F8> :TagbarToggle<CR>

"Vim quickfix
nnoremap <Down> :cn<CR>
nnoremap <Up> :cp<CR>
nnoremap <Left> :cpf<CR>
nnoremap <Right> :cnf<CR>

nnoremap <leader>s :call FixLastSpellingError()<cr>
nnoremap <leader>S z=
"---------------------End Key Maps----------------------------"


" ------------Vundle plugin Start-----------------------------"""

call plug#begin('~/.vim/plugged')

" Plug 'tpope/vim-fugitive'
" Plug 'scrooloose/nerdtree'
" Plug 'jiangmiao/auto-pairs'
" Plug 'easymotion/vim-easymotion'
" Plug 'alvan/vim-closetag'
" Plug 'rstacruz/sparkup', {'rtp': 'vim/'} 
" Plug 'thinca/vim-quickrun'
" Plug 'xolox/vim-notes'
" Plug 'WolfgangMehner/c-support.git'
" Asynchronous Lint Engine
" Plug 'w0rp/ale'
" Enable autoclose
" Plug 'Raimondi/delimitMate'
" Show git diffs
" Plug 'airblade/vim-gitgutter'
" Folding Tex files
" Plug 'matze/vim-tex-fold', {'for': 'tex'}
" Supoort for html/css
" Plug 'mattn/emmet-vim', {'for': ['html', 'css']}
" "------- vim-pandoc
" Plug 'vim-pandoc/vim-pandoc' , {'for': 'pdc'}
" Plug 'vim-pandoc/vim-pandoc-syntax', {'for': 'pdc'}
" Plug 'vim-pandoc/vim-pandoc-after', {'for': 'pdc'}
"------------- Code completion------YCM
" Plug 'Valloric/YouCompleteMe', {'do': 'python3 install.py --clang-completer --java-completer --ts-completer'}
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tomtom/tcomment_vim'
Plug 'xolox/vim-misc'

" Resize Mode
Plug 'simeji/winresizer'

" Latex
Plug 'lervag/vimtex', {'for': 'tex'}
" ------- vim-markdown-composer
Plug 'euclio/vim-markdown-composer', {'for': 'md'}

" snippets engine for various languages (needed for ultisnips)
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
" ES2015 code snippets (Optional)
Plug 'epilande/vim-es2015-snippets'
" React code snippets
Plug 'epilande/vim-react-snippets'

" clang-format code for c familly
Plug 'rhysd/vim-clang-format', {'for': ['c', 'cpp']}
" Vim bindings for rtags, llvm/clang based c++ code indexer.
Plug 'lyuts/vim-rtags', {'for': ['cpp', 'h']}

" React JSX syntax highlighting and indenting
Plug 'mxw/vim-jsx'
Plug 'pangloss/vim-javascript'

" a fuzzy file finder
Plug 'junegunn/fzf.vim'
" Vim Undo tree visualizer
Plug 'simnalamburt/vim-mundo'
" NerdTree
Plug 'scrooloose/nerdtree'

" Enable autoclose
Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-surround'
" surround.vim: quoting/parenthesizing made simple
Plug 'tpope/vim-surround'
" A Vim alignment plugin
Plug 'junegunn/vim-easy-align'

" Vim plugin for copying to the system clipboard with text-objects and motions
Plug 'christoomey/vim-system-copy'

" gruvbox colorscheme
Plug 'morhetz/gruvbox'
Plug 'cohlin/vim-colorschemes'
Plug 'saalaa/ancient-colors.vim'
Plug 'joshdick/onedark.vim'
Plug 'vim-syntastic/syntastic'

"Vim ColorSchemes
Plug 'pbrisbin/vim-colors-off'
Plug 'fxn/vim-monochrome'

" Vim plugin that displays tags in a window, ordered by scope
Plug 'majutsushi/tagbar'

" Vim jump to places
Plug 'wincent/vcs-jump'

"Intellisense engine for vim8, full language server protocol support as VSCode
Plug 'neoclide/coc.nvim', {'branch': 'release'}



call plug#end()
" ------------Vundle END--------------------------------"""

" ------------Relative numbering------------------------"""
"Toggle relative numbering, 
"and set to absolute on loss of focus or insert mode
set rnu
function! ToggleNumbersOn()
set nu!
set rnu
endfunction
function! ToggleRelativeOn()
set rnu!
set nu
endfunction
autocmd FocusLost * call ToggleRelativeOn()
autocmd FocusGained * call ToggleRelativeOn()
autocmd InsertEnter * call ToggleRelativeOn()
autocmd InsertLeave * call ToggleRelativeOn()
" -----------End Relative Numbering------------------------""""

"----------------------Sparkup-----------------------"
let g:sparkupNextMapping = '<c-l>'
"----------------------End Sparkup-----------------------"

"----------------UltiSnips---------------------------------"
" let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsJumpForwardTrigger="<c-l>"
let g:UltiSnipsJumpBackwardTrigger="<c-h>"
let g:UltiSnipsExpandTrigger = "<nop>"
let g:ulti_expand_or_jump_res = 0
function ExpandSnippetOrCarriageReturn()
  let snippet = UltiSnips#ExpandSnippetOrJump()
  if g:ulti_expand_or_jump_res > 0
      return snippet
  else
      return "\<CR>"
  endif
endfunction
inoremap <expr> <CR> pumvisible() ? "<C-R>=ExpandSnippetOrCarriageReturn()<CR>" : "\<CR>"
"----------------End UltiSnips---------------------------------"

"----------------Vim-Airline---------------------------------"
" change theme
let g:airline_theme='solarized_flood'
"----------------END Vim-Airline---------------------------------"

"----------------markdown-composer---------------------------------"
let g:markdown_composer_custom_css=['https://jasonm23.github.io/markdown-css-themes/markdown1.css']
let g:markdown_composer_syntax_theme='darcula'
let g:markdown_composer_external_renderer='pandoc -f markdown --toc --toc-depth=3 -s -t html'
let g:markdown_composer_autostart=0
" let g:markdown_composer_refresh_rate=0
" let g:markdown_composer_browser='firefox'
"---------------END markdown-composer---------------------------------"

"----------------vim-rtags---------------------------------"
let g:rtagsUseLocationList=0
"----------------END vim-rtags---------------------------------"

"----------------EasyAlign---------------------------------"
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)
"----------------END EasyAlign---------------------------------"

" ----------------------AirlineTheme-----------------------"
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline_theme='powerlineish'
" ----------------------End AirlineTheme-----------------------"

" ----------------------Vimtex-----------------------"
let g:tex_flavor='latex'
let g:vimtex_view_method='zathura'
let g:vimtex_quickfix_mode=0
set conceallevel=1
let g:tex_conceal='abdmg'
"------------------VIM Vimtex------------------------

"------------------COC------------------------
" if hidden is not set, TextEdit might fail.
set hidden
" Some servers have issues with backup files, see #649
set nobackup
set nowritebackup
" Better display for messages
set cmdheight=2
" You will have bad experience for diagnostic messages when it's default 4000.
set updatetime=300
" don't give |ins-completion-menu| messages.
set shortmess+=c
" always show signcolumns
set signcolumn=yes
" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction
" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()
" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" Or use `complete_info` if your vim support it, like:
" inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)
" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>
function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction
" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')
" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)
" Remap for format selected region
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)
augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end
" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
xmap <leader>ac  <Plug>(coc-codeaction-selected)
nmap <leader>ac  <Plug>(coc-codeaction-selected)
" Remap for do codeAction of current line
nmap <leader>a  <Plug>(coc-codeaction)
" Fix autofix problem of current line
nmap <space>f  <Plug>(coc-fix-current)
" Create mappings for function text object, requires document symbols feature of languageserver.
xmap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap if <Plug>(coc-funcobj-i)
omap af <Plug>(coc-funcobj-a)
" Use <C-d> for select selections ranges, needs server support, like: coc-tsserver, coc-python
nmap <silent> <C-d> <Plug>(coc-range-select)
xmap <silent> <C-d> <Plug>(coc-range-select)
" Use `:Format` to format current buffer
command! -nargs=0 Format :call CocAction('format')
" Use `:Fold` to fold current buffer
command! -nargs=? Fold :call     CocAction('fold', <f-args>)
" use `:OR` for organize import of current buffer
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')
" Add status line support, for integration with other plugin, checkout `:h coc-status`
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}
" Using CocList
" Show all diagnostics
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>
"------------------END COC------------------------

"------------------VIM FUNCTIONS------------------------
function! FixLastSpellingError()
  normal! mm[s1z=`m
endfunction 
"------------------END-VIM FUNCTIONS------------------------

"---------------------General Settings------------------------""
set nu
syntax on
set termguicolors
set textwidth=0
" colorscheme onedark
colorscheme paramount
let &colorcolumn='120'
" let g:gruvbox_italic=1
" colorscheme gruvbox
set tabstop=2
set shiftwidth=2 
set autoindent   
set smartindent  
set cindent
set laststatus=2
set ttimeoutlen=0
set undofile " Maintain undo history between sessions
set undodir=~/.vim/undodir
set incsearch
set hlsearch
let g:tex_flavor = "latex"
set encoding=utf-8
set ignorecase
" Disable modelines for security reasons
set nomodeline
set modelines=0
"Map caps to Escape at opening vim (to make sure it's always there)
" silent ! xmodmap -e 'clear Lock' -e 'keycode 0x42 = Escape'
":autocmd BufEnter * lcd %:p:h
"
"---------------------End General Settings------------------------""
autocmd BufReadPost *
  \ if line("'\"") >= 1 && line("'\"") <= line("$") |
  \   exe "normal! g`\"" |
  \ endif

"----------------ALE---------------------------------"
"" Set this. Airline will handle the rest.
" let g:airline#extensions#ale#enabled = 1  
" let g:ale_completion_enabled = 0
" let g:ale_lint_on_save = 0
" let g:ale_lint_on_text_changed = 'never'
" " if you don't want linters to run on opening a file
" let g:ale_lint_on_enter = 0
" let g:ale_linters = {'cpp': ['clangtidy']}
"----------------END ALE---------------------------------"
"
"----------------vim-gitgutter---------------------------------"
" let g:gitgutter_enabled = 0
" set updatetime=100
"----------------END vim-gitgutter---------------------------------"
"
"----------------Vim-Pandoc---------------------------------"
" let g:pandoc#after#modules#enabled = ["ultisnips"]
" " pandoc , markdown
" command! -nargs=* RunSilent
"       \ | execute ':silent !'.'<args>'
"       \ | execute ':redraw!'
" nmap <Leader>pc :w <bar> :RunSilent pandoc -o /tmp/vim-pandoc-out.pdf %<CR>
" nmap <Leader>pp :RunSilent zathura /tmp/vim-pandoc-out.pdf &<CR>
"----------------END Vim-Pandoc---------------------------------"
"
" ----------------Syntastic---------------------------------"
" set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
" set statusline+=%*
"
" let g:syntastic_always_populate_loc_list = 1
" let g:syntastic_auto_loc_list = 0
" let g:syntastic_check_on_open = 0
" let g:syntastic_check_on_wq = 0
" let g:syntastic_aggregate_errors = 1
" let g:syntastic_python_checkers = ['pylint', 'flake8', 'bandit']
" let g:syntastic_mode_map = { 'mode': 'passive' }
" ----------------End Syntastic--------------------------"
"
"------------------You complete me---------------------"
" let g:ycm_confirm_extra_conf = 1
" let g:ycm_global_ycm_extra_conf = '~/.ycm_extra_conf.py'
" let g:ycm_show_diagnostics_ui = 1
" let g:ycm_seed_identifiers_with_syntax = 1
" set completeopt-=preview
" let g:ycm_always_populate_location_list = 1
" let g:syntastic_error_symbol = '✗'
" let g:syntastic_warning_symbol = '⚠'
" let g:ycm_add_preview_to_completeopt = 0
" let g:EclimFileTypeValidate = 0
" let g:ycm_enable_diagnostic_highlighting = 0
"             " \ 'tex'  : ['{', '\']
" let g:ycm_semantic_triggers = {
"             \ 'tex'  : [
"             \ 're!\\[A-Za-z]*cite[A-Za-z]*(\[[^]]*\]){0,2}{[^}]*',
"             \ 're!\\[A-Za-z]*ref({[^}]*|range{([^,{}]*(}{)?))',
"             \ 're!\\hyperref\[[^]]*',
"             \ 're!\\includegraphics\*?(\[[^]]*\]){0,2}{[^}]*',
"             \ 're!\\(include(only)?|input){[^}]*',
"             \ 're!\\\a*(gls|Gls|GLS)(pl)?\a*(\s*\[[^]]*\]){0,2}\s*\{[^}]*',
"             \ 're!\\includepdf(\s*\[[^]]*\])?\s*\{[^}]*',
"             \ 're!\\includestandalone(\s*\[[^]]*\])?\s*\{[^}]*',
"             \ 're!\\usepackage(\s*\[[^]]*\])?\s*\{[^}]*',
"             \ 're!\\documentclass(\s*\[[^]]*\])?\s*\{[^}]*',
"             \ 're!\\[A-Za-z]*',
"             \ ],
"             \ 'html': ['<', '</'],
"             \ 'css': [':'],
"             \}
"
"------------------End You complete me---------------------"
