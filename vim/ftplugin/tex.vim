nnoremap <F6> :w<CR> <bar> :VimtexClean<CR> <bar> :VimtexReload<CR>
nnoremap <F8> :w<CR> <bar> :VimtexCompile<CR>
" reformat text to respect textwidth
nnoremap <leader>f :normal! gq2j<cr>
let b:syntastic_mode="passive"
" set textwidth=75
if !exists('g:ycm_semantic_triggers')
  let g:ycm_semantic_triggers = {}
endif
" let g:ycm_semantic_triggers.tex = g:vimtex#re#youcompleteme
" Save the fold upon exit and return it when open the file
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview 
let g:vimtex_quickfix_ignore_all_warnings=0
set spell spelllang=en_us
" Spellchecker segestion
nnoremap <leader>s :call FixLastSpellingError()<cr>
inoremap <C-l> <Esc>:call FixLastSpellingError()<cr>a
nnoremap <leader>S z=
set spell
