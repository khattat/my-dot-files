" Vim syntax file
" Language: Celestia Star Catalogs
" Maintainer: Kevin Lauder
" Latest Revision: 26 April 2008

if exists("b:current_syntax")
  finish
endif


syn match ops 'num='
syn match ops 'num<'
syn match ops 'num>'
syn match ops '+'
syn match ops '-'
syn match ops '*'

syn match num '[0-9]\+'
syn match num '[+-][0-9]\+'

syn match reclam 'rec-lam'

syn keyword conditionals if 
syn keyword basickeywords lambda tuple pair proj
syn keyword booleans true false
syn keyword labels let



let b:current_syntax = "mlsp"

hi def link num Constant
hi def link conditionals Conditional
hi def link basickeywords Keyword
hi def link reclam Keyword
hi def link booleans Boolean
hi def link ops Operator
hi def link labels Typedef

